/**
 * 
 */



function stampaProdotti(arrProdotti) {
	let contenuto = "";

	for (let i = 0; i < arrProdotti.length; i++) {
		contenuto += prodottoTabellataRisultato(arrProdotti[i]);
	}

	$("#contenuto-richiesta").html(contenuto);
}





function prodottoTabellataRisultato(obj_prodotto) {

	let risultato = '<tr data-identificatore="' + obj_prodotto.idProdotto + '">';
	risultato += '    <td>' + obj_prodotto.nomeProdotto + '</td>';
	risultato += '    <td>' + obj_prodotto.descrizioneProdotto.substr(0, 60) + '...</td>';
    risultato += '    <td>' + obj_prodotto.codiceOggetto + '</td>';
    risultato += '    <td>' + obj_prodotto.prezzoVendita + '</td>';
	risultato += '    <td><button type="button" class="btn btn-danger btn-block" onclick="eliminaProdotto(this)">DELETE</button></td>';
	risultato += '    <td><button type="button" class="btn btn-warning btn-block" onclick="modificaProdotto(this)">EDIT</button></td>';
	risultato += '</tr>';

	return risultato;

}



function aggiornaTabella() {
	$.ajax(
		{
			url: "http://localhost:8080/ProgettoMobilificio/recuperaprodotti",
			method: "POST",
			success: function(ris_success) {



				stampaProdotti(ris_success);

			},
			error: function(ris_error) {
				console.log("ERRORE");
				console.log(ris_error);
			}
		}
	);
}




function eliminaProdotto(objButton) {
	let idSelezionato = $(objButton).parent().parent().data("identificatore");
	

	$.ajax(
		{
			url: "http://localhost:8080/ProgettoMobilificio/rimuoviprodotto",
			method: "POST",
			data: {
				Prodottoid: idSelezionato
			},
			success: function(responso) {			
				switch (responso.risultato) {
					case "OK":
						aggiornaTabella();
						alert("Eliminato con successo");
						break;
					case "ERRORE":
						alert("ERRORE DI ELIMINAZIONE :(\n" + responso.dettaglio);
						break;
				}

			},
			error: function(errore) {
				console.log(errore);
			}
		}
	);



}






function modificaProdotto(objButton){
	let idSelezionato = $(objButton).parent().parent().data("identificatore");
	
	$.ajax(
			{
				url: "http://localhost:8080/ProgettoMobilificio/aggiornaprodotto",
				method: "POST",
				data: {
					id_prodotto: idSelezionato
				},
				success: function(responso){			
					
					$("#input_nome").val(responso.nomeProdotto);
					$("#input_descrizione").val(responso.descrizioneProdotto);
					$("#input_codice").val(responso.codiceOggetto);
					$("#input_prezzo").val(responso.prezzoVendita);
					
					$("#modaleModifica").data("identificatore", idSelezionato);

					$("#modaleModifica").modal("show");
					
					
				},
				error: function(errore){
					console.log(errore);
				}
			}
	);
}
























$(document).ready(
	function() {



		window.setInterval(
			function() {
				aggiornaTabella();
				console.log("AGGIORNATO");
			}
			, 5000);


		$("#cta-leggi").click(
			function() {
				aggiornaTabella();
			}



		);
		$("#cta-aggiungi").click(
			function() {
				$("#modaleInserimento").modal("show");
			}
		);



		$("#cta-inserisci").click(
			function() {
				let input_nome = $("#nomeC").val();
				let input_descrizione = $("#input_descrizione").val();
				let input_codice = $("#codiceC").val();
				let input_prezzo =$("#prezzoC").val();

				$.ajax(
					{
						url: "http://localhost:8080/ProgettoMobilificio/inserisciprodotto",
						method: "POST",
						data: {
							nomeP: input_nome,
							descrizioneP: input_descrizione,
							codiceP: input_codice,
							prezzoP : input_prezzo
						},
						success: function(risultato) {
							console.log("SONO IN SUCCESS");
							let risJson = JSON.parse(risultato);

							switch (risJson.risultato) {
								case "OK":
									aggiornaTabella();
									$("#modaleInserimento").modal("toggle");
									
									
									alert("Inserito con successo");
									break;
								case "ERRORE":
									alert("ERRORE DI INSERIMENTO :(\n" + risJson.dettaglio);
									break;
							}

							console.log(risJson);
						},
						error: function(errore) {
							console.log("SONO IN ERROR");
							let errJson = JSON.parse(errore);
							console.log(errJson);
						}

					}
				);

			}
		);





        $("#cta-modifica").click(
            function(){
                let id_prodotto = $("#modaleModifica").data("identificatore");
				let campoNome= $("#input_nome").val();
				let campoDescrizione =  $("#input_descrizione").val();
				let campoCodice = $("#input_codice").val();
				let campoPrezzo = $("#input_prezzo").val();

                $.ajax(
                        {
                            url: "http://localhost:8080/ProgettoMobilificio/aggiornaprodotto",
                            method: "POST",
                            data: {
                                id_prodotto: id_prodotto,
                                nomeP: campoNome,
                                descrizioneP :campoDescrizione,
                                codiceP : campoCodice,
                                prezzoP : campoPrezzo
                                
                                
                            },
                            success: function(responso){
                                switch(responso.risultato){
                                case "OK":
                                  aggiornaTabella();
                                    $("#modaleModifica").modal("toggle");
                                    aggiornaTabella();
                                    break;
                                case "ERRORE":
                                    alert(responso.risultato + "\n" + responso.dettaglio);
                                    break;
                                }
                            },
                            error: function(errore){
                                console.log(errore);
                            }
                        }
                );
                
            }
    );
    
    
}
);





