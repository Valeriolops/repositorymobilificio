/**
 * 
 */
 function stampaCategorie(arrCategorie) {
	let contenuto = "";

	for (let i = 0; i < arrCategorie.length; i++) {
		contenuto += categoriaTabellataRisultato(arrCategorie[i]);
	}

	$("#contenuto-richiesta").html(contenuto);
}

function categoriaTabellataRisultato(obj_categoria) {

	let risultato = '<tr data-identificatore="' + obj_categoria.idCategoria + '">';
	risultato += '    <td>' + obj_categoria.nomeCategoria + '</td>';
	risultato += '    <td>' + obj_categoria.descrizione.substr(0, 60) + '...</td>';
	risultato += '    <td><button type="button" class="btn btn-danger btn-block" onclick="eliminaCategoria(this)">DELETE</button></td>';
	risultato += '    <td><button type="button" class="btn btn-warning btn-block" onclick="modificaCategoria(this)">EDIT</button></td>';
	risultato += '</tr>';

	return risultato;

}





function aggiornaTabella() {
	$.ajax(
		{
			url: "http://localhost:8080/ProgettoMobilificio/recuperacategorie",
			method: "POST",
			success: function(ris_success) {



				stampaCategorie(ris_success);

			},
			error: function(ris_error) {
				console.log("ERRORE");
				console.log(ris_error);
			}
		}
	);
}

function eliminaCategoria(objButton) {
	let idSelezionato = $(objButton).parent().parent().data("identificatore");
	console.log(idSelezionato);

	$.ajax(
		{
			url: "http://localhost:8080/ProgettoMobilificio/rimuovicategoria",
			method: "POST",
			data: {
				Categoriaid: idSelezionato
			},
			success: function(responso) {			
				switch (responso.risultato) {
					case "OK":
						aggiornaTabella();
						alert("Eliminato con successo");
						break;
					case "ERRORE":
						alert("ERRORE DI ELIMINAZIONE :(\n" + responso.dettaglio);
						break;
				}

			},
			error: function(errore) {
				console.log(errore);
			}
		}
	);



}



function modificaCategoria(objButton){
	let idSelezionato = $(objButton).parent().parent().data("identificatore");
	
	$.ajax(
			{
				url: "http://localhost:8080/ProgettoMobilificio/aggiornacategoria",
				method: "POST",
				data: {
					id_articolo: idSelezionato
				},
				success: function(responso){			
					
					$("#input_nome").val(responso.nome);
					$("#input_descrizione").val(responso.descrizione);
					$("#modaleModifica").data("identificatore", idSelezionato);

					$("#modaleModifica").modal("show");
					
					
				},
				error: function(errore){
					console.log(errore);
				}
			}
	);
}















$(document).ready(
	function() {



		window.setInterval(
			function() {
				aggiornaTabella();
				console.log("AGGIORNATO");
			}
			, 3000);


		$("#cta-leggi").click(
			function() {
				aggiornaTabella();
			}



		);
		$("#cta-aggiungi").click(
			function() {
				$("#modaleInserimento").modal("show");
			}
		);



		$("#cta-inserisci").click(
			function() {
				let input__nomes = $("#nomeC").val();
				let input__descrizione = $("#input_descrizione").val();

				$.ajax(
					{
						url: "http://localhost:8080/ProgettoMobilificio/inseriscicategoria",
						method: "POST",
						data: {
							input_nome: input__nomes,
							input_descrizione: input__descrizione
						},
						success: function(risultato) {
							console.log("SONO IN SUCCESS");
							let risJson = JSON.parse(risultato);

							switch (risJson.risultato) {
								case "OK":
									aggiornaTabella();
									$("#modaleInserimento").modal("toggle");
									
									
									alert("Inserito con successo");
									break;
								case "ERRORE":
									alert("ERRORE DI INSERIMENTO :(\n" + risJson.dettaglio);
									break;
							}

							console.log(risJson);
						},
						error: function(errore) {
							console.log("SONO IN ERROR");
							let errJson = JSON.parse(errore);
							console.log(errJson);
						}

					}
				);

			}
		);





        $("#cta-modifica").click(
            function(){
                let idSelezionato = $("#modaleModifica").data("identificatore");
				let campoNome= $("#edit_nome").val();
				let campoDescrizione =  $("#edit_descrizione").val();
	

                $.ajax(
                        {
                            url: "http://localhost:8080/ProgettoMobilificio/aggiornacategoria",
                            method: "POST",
                            data: {
                                id_selezionato: idSelezionato,
                                nome: campoNome,
                                descrizione:campoDescrizione
                            },
                            success: function(responso){
                                switch(responso.risultato){
                                case "OK":
                                    $("#modaleModifica").modal("toggle");
                                    aggiornaTabella();
                                    break;
                                case "ERRORE":
                                    alert(responso.risultato + "\n" + responso.dettaglio);
                                    break;
                                }
                            },
                            error: function(errore){
                                console.log(errore);
                            }
                        }
                );
                
            }
    );
    
    
}
);



