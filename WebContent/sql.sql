
drop database if exists mobilificio;
create database mobilificio;
use mobilificio;

create table categoria(
idCategoria integer not null auto_increment primary key,
nomeCategoria varchar (250) not null unique,
descrizione varchar(250)  default " nessuna descrizione" );


insert into categoria(nomeCategoria) values (
"elettrodomestico"),
("sedie"),
("tavoli");


select idCategoria, nomeCategoria,  descrizione  from categoria;


insert into Categoria  (nomeCategoria, descrizione ) value ( "Armadi" ," armadi in legno");


select idCategoria, nomeCategoria,  descrizione  from categoria  where idCategoria = 2;


create table Prodotto (
idProdotto integer not null auto_increment primary key,
nomeProdotto varchar (250) not null unique ,
descrizioneProdotto varchar (250) default "nessuna" ,
codiceOggetto varchar (250) ,
prezzoVendita double );


select idProdotto, nomeProdotto,descrizioneProdotto,codiceOggetto,prezzoVendita from Prodotto where idProdotto =2;






insert into Prodotto( nomeProdotto,codiceOggetto,prezzoVendita) values (
"Lavatrice","123LAV",250),
("Sedia per pranzo", "123SED",40),
("Tavolo Giardino", "123TAV", 20);

insert into Prodotto  (nomeProdotto,descrizioneProdotto, codiceOggetto,prezzoVendita ) value ( "cellulare" ,  "smartphone" ,"Cell123", 500);

select * from Prodotto;


create table ProdottoCategoria ( 
codiceProdotto integer not null,
codiceCategoria integer not null,
 foreign key (codiceProdotto) references Prodotto( idProdotto) on delete cascade,
 foreign key (codiceCategoria) references categoria (idCategoria) on delete cascade,
 primary key (codiceProdotto , codiceCategoria));






