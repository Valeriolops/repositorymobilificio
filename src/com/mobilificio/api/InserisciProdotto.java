package com.mobilificio.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.mobilificio.model.Prodotto;
import com.mobilificio.services.ProdootoDAO;
import com.mobilificio.utility.ResponsoOperazione;

/**
 * Servlet implementation class InserisciProdotto
 */
@WebServlet("/inserisciprodotto")
public class InserisciProdotto extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		
		String nomeP = request.getParameter("nomeP") != null ? request.getParameter("nomeP") : "";
		String descrizioneP = request.getParameter("descrizioneP") != null ? request.getParameter("descrizioneP") : "";
		String codiceP = request.getParameter("codiceP") != null ? request.getParameter("codiceP") : "";
		Double prezzoP = request.getParameter("prezzoP") != null ? Double.parseDouble(request.getParameter("prezzoP")) : -1;
		
		
		Prodotto pTemp = new Prodotto();
		pTemp.setNomeProdotto(nomeP);
		pTemp.setDescrizioneProdotto(descrizioneP);
		pTemp.setCodiceOggetto(codiceP);
		pTemp.setPrezzoVendita(prezzoP);
		
		ProdootoDAO pDao = new ProdootoDAO();
		String strErrore ="";
		
		
		
		
		try {
			pDao.insert(pTemp);
		} catch (SQLException e) {
			strErrore = e.getMessage();
		}

		PrintWriter out = response.getWriter();
		Gson jsonizzatore = new Gson();

		if (pTemp.getIdProdotto() != null) {

			ResponsoOperazione res = new ResponsoOperazione("OK", "");
			out.print(jsonizzatore.toJson(res));
		} else {
			ResponsoOperazione res = new ResponsoOperazione("ERRORE", strErrore);
			out.print(jsonizzatore.toJson(res));
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}

}
