package com.mobilificio.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.mobilificio.model.Categoria;
import com.mobilificio.services.CategoriaDAO;
import com.mobilificio.utility.ResponsoOperazione;

@WebServlet("/rimuovicategoria")
public class RimuoviCategoria extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		PrintWriter out = response.getWriter();
		response.setContentType("application/json"); 
		response.setCharacterEncoding("UTF-8");
		Gson jsonizzatore = new Gson();

		Integer identificatore = request.getParameter("Categoriaid") != null
				? Integer.parseInt(request.getParameter("Categoriaid"))
				: -1;

		if (identificatore != -1) {

			CategoriaDAO cDao = new CategoriaDAO();

			try {

				Categoria cTemp = cDao.getById(identificatore);
				if (cDao.delete(cTemp)) {
					ResponsoOperazione res = new ResponsoOperazione("OK", "");
					out.print(jsonizzatore.toJson(res));
				} else {
					ResponsoOperazione res = new ResponsoOperazione("ERRORE",
							"Nono sono riuscito a fare l'eliminazione");
					out.print(jsonizzatore.toJson(res));
				}

			} catch (SQLException e) {
				ResponsoOperazione res = new ResponsoOperazione("ERRORE", e.getMessage());
				out.print(jsonizzatore.toJson(res));
			}

		} else {
			ResponsoOperazione res = new ResponsoOperazione("ERRORE", "Id non selezionato!");
			out.print(jsonizzatore.toJson(res));
		}

	}

}
