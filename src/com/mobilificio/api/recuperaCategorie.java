package com.mobilificio.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import com.mobilificio.model.Categoria;
import com.mobilificio.services.CategoriaDAO;
import com.mobilificio.utility.ResponsoOperazione;

/**
 * Servlet implementation class recuperaCategorie
 */
@WebServlet("/recuperacategorie")
public class recuperaCategorie extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");

		CategoriaDAO cDao = new CategoriaDAO();

		Integer identificatore = request.getParameter("id_categoria") != null
				? Integer.parseInt(request.getParameter("id_categoria"))
				: -1;

		try {

			if (identificatore == -1) {
				ArrayList<Categoria> elencoCategorie = cDao.getAll();

				String risultatoJson = new Gson().toJson(elencoCategorie);
				Thread.sleep(500);
				out.print(risultatoJson);

			} else {

				Categoria cTemp = cDao.getById(identificatore);
				out.print(new Gson().toJson(cTemp));
			}
		} catch (SQLException | InterruptedException e) {

			out.print(new Gson().toJson(new ResponsoOperazione("ERRORE", e.getMessage()))); 
			System.out.println(e.getMessage());

		}

	}

}
