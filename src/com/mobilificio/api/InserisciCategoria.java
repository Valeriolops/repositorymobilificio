package com.mobilificio.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.mobilificio.model.Categoria;
import com.mobilificio.services.CategoriaDAO;
import com.mobilificio.utility.ResponsoOperazione;

/**
 * Servlet implementation class InserisciCategoria
 */
@WebServlet("/inseriscicategoria")
public class InserisciCategoria extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String nomeCategoria = request.getParameter("input_nome");
		String varDescrizione = request.getParameter("input_descrizione");

		Categoria cTemp = new Categoria();
		cTemp.setNomeCategoria(nomeCategoria);
		cTemp.setDescrizione(varDescrizione);
		CategoriaDAO cDao = new CategoriaDAO();
		String strErrore = "";

		try {
			cDao.insert(cTemp);
		} catch (SQLException e) {
			strErrore = e.getMessage();
		}

		PrintWriter out = response.getWriter();
		Gson jsonizzatore = new Gson();

		if (cTemp.getIdCategoria() != null) {

			ResponsoOperazione res = new ResponsoOperazione("OK", "");
			out.print(jsonizzatore.toJson(res));
		} else {
			ResponsoOperazione res = new ResponsoOperazione("ERRORE", strErrore);
			out.print(jsonizzatore.toJson(res));
		}

	}

}
