package com.mobilificio.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.mobilificio.model.Categoria;
import com.mobilificio.model.Prodotto;
import com.mobilificio.services.CategoriaDAO;
import com.mobilificio.services.ProdootoDAO;
import com.mobilificio.utility.ResponsoOperazione;

/**
 * Servlet implementation class RecuperaProdotti
 */
@WebServlet("/recuperaprodotti")
public class RecuperaProdotti extends HttpServlet {
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		

		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");

		ProdootoDAO pDao = new ProdootoDAO();

		Integer identificatore = request.getParameter("id_prodotto") != null
				? Integer.parseInt(request.getParameter("id_prodotto"))
				: -1;

		try {

			if (identificatore == -1) {
				ArrayList<Prodotto> elencoProdotti = pDao.getAll();

				String risultatoJson = new Gson().toJson(elencoProdotti);
				Thread.sleep(500);
				out.print(risultatoJson);

			} else {

				Prodotto pTemp = pDao.getById(identificatore);
				out.print(new Gson().toJson(pTemp));
			}
		} catch (SQLException | InterruptedException e) {

			out.print(new Gson().toJson(new ResponsoOperazione("ERRORE", e.getMessage()))); 
			System.out.println(e.getMessage());

		}
		
		
		
		
		
		
		
	}

}
