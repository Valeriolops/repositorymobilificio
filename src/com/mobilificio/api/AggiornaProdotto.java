package com.mobilificio.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.mobilificio.model.Prodotto;
import com.mobilificio.services.ProdootoDAO;
import com.mobilificio.utility.ResponsoOperazione;

/**
 * Servlet implementation class AggiornaProdotto
 */
@WebServlet("/aggiornaprodotto")
public class AggiornaProdotto extends HttpServlet {
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Integer idProdotto = request.getParameter("id_prodotto") != null ? Integer.parseInt(request.getParameter("id_prodotto")) : -1;
		String nomeP = request.getParameter("nomeP") != null ? request.getParameter("nomeP") : "";
		String descrizioneP = request.getParameter("descrizioneP") != null ? request.getParameter("descrizioneP") : "";
		String codiceP = request.getParameter("codiceP") != null ? request.getParameter("codiceP") : "";
		Double prezzoP = request.getParameter("prezzoP") != null ? Double.parseDouble(request.getParameter("prezzoP")) : -1;
		
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		
		ProdootoDAO pDao = new ProdootoDAO();
	
		
		if(idProdotto == -1 || nomeP.trim().equals("")) {
			out.print(new Gson().toJson(new ResponsoOperazione("ERRORE", "ID o NOME non inseriti!")));
			return;
		}
		
		try {
			Prodotto pTemp = pDao.getById(idProdotto);
			pTemp.setNomeProdotto(nomeP);
			pTemp.setDescrizioneProdotto(descrizioneP);
			pTemp.setCodiceOggetto(codiceP);
			pTemp.setPrezzoVendita(prezzoP);
			
			if(pDao.update(pTemp)) {
				out.print(new Gson().toJson(new ResponsoOperazione("OK", "")));
				return;
			}
			
			out.print(new Gson().toJson(new ResponsoOperazione("ERRORE", "Modifica non effettuata!")));

			
		} catch (SQLException e) {
			out.print(new Gson().toJson(new ResponsoOperazione("ERRORE", "Prodotto non trovato!")));
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}

}
