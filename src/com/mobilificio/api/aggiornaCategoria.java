package com.mobilificio.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.mobilificio.model.Categoria;
import com.mobilificio.services.CategoriaDAO;
import com.mobilificio.utility.ResponsoOperazione;

/**
 * Servlet implementation class aggiornaCategoria
 */
@WebServlet("/aggiornacategoria")
public class aggiornaCategoria extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		Integer idArticolo = request.getParameter("id_selezionato") != null ? Integer.parseInt(request.getParameter("id_selezionato")) : -1;
		String nomeCa = request.getParameter("nome") != null ? request.getParameter("nome") : "";
		String descrizioneArticolo = request.getParameter("descrizione") != null ? request.getParameter("descrizione") : "";
		
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		CategoriaDAO cDao = new CategoriaDAO();
		
		if(idArticolo == -1 || nomeCa.trim().equals("")) {
			out.print(new Gson().toJson(new ResponsoOperazione("ERRORE", "ID o Nome non inseriti!")));
			return;
		}
		
		try {
			Categoria cTemp = cDao.getById(idArticolo);
			
			cTemp.setNomeCategoria(nomeCa);
			cTemp.setDescrizione(descrizioneArticolo);
			
			if(cDao.update(cTemp)) {
				out.print(new Gson().toJson(new ResponsoOperazione("OK", "")));
				return;
			}
			
			out.print(new Gson().toJson(new ResponsoOperazione("ERRORE", "Modifica non effettuata!")));

			
		} catch (SQLException e) {
			out.print(new Gson().toJson(new ResponsoOperazione("ERRORE", "Categoria non trovata!")));
		}

		
		
		
		
		
	}

}
