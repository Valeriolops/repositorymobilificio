package com.mobilificio.model;

import java.util.ArrayList;

public class Prodotto {
	
	
	
	
	private Integer idProdotto;
	private String nomeProdotto;
	private String descrizioneProdotto;
	private String codiceOggetto;
	private Double prezzoVendita;
	public ArrayList<Categoria>listaCategorie = new ArrayList<Categoria>();
	
	
	public Prodotto() {
		
	}



	public Prodotto(Integer idProdotto, String nomeProdotto, String descrizioneProdotto, String codiceOggetto,
			Double prezzoVendita) {
		
		this.idProdotto = idProdotto;
		this.nomeProdotto = nomeProdotto;
		this.descrizioneProdotto = descrizioneProdotto;
		this.codiceOggetto = codiceOggetto;
		this.prezzoVendita = prezzoVendita;
	}

	
	
	public void addCategoriaAlProdotto(Categoria c) {
		this.listaCategorie.add(c);
		
	}
	
	public ArrayList<Categoria> getTutteLeCategorieDelProdotto(){
		return this.listaCategorie;
	}
	
	


	public Integer getIdProdotto() {
		return idProdotto;
	}



	public void setIdProdotto(Integer idProdotto) {
		this.idProdotto = idProdotto;
	}



	public String getNomeProdotto() {
		return nomeProdotto;
	}



	public void setNomeProdotto(String nomeProdotto) {
		this.nomeProdotto = nomeProdotto;
	}



	public String getDescrizioneProdotto() {
		return descrizioneProdotto;
	}



	public void setDescrizioneProdotto(String descrizioneProdotto) {
		this.descrizioneProdotto = descrizioneProdotto;
	}



	public String getCodiceOggetto() {
		return codiceOggetto;
	}



	public void setCodiceOggetto(String codiceOggetto) {
		this.codiceOggetto = codiceOggetto;
	}



	public Double getPrezzoVendita() {
		return prezzoVendita;
	}



	public void setPrezzoVendita(Double prezzoVendita) {
		this.prezzoVendita = prezzoVendita;
	}
	
	
	
	
	
	
	
	
	
	
	

}
