package com.mobilificio.model;

public class Categoria {
	
	
	private Integer idCategoria;
	private String nomeCategoria;
	private String descrizione;
	
	
	public Categoria() {
		
	}


	public Categoria(Integer idCategoria, String nomeCategoria, String descrizione) {
		
		this.idCategoria = idCategoria;
		this.nomeCategoria = nomeCategoria;
		this.descrizione = descrizione;
	}


	public Integer getIdCategoria() {
		return idCategoria;
	}


	public void setIdCategoria(Integer idCategoria) {
		this.idCategoria = idCategoria;
	}


	public String getNomeCategoria() {
		return nomeCategoria;
	}


	public void setNomeCategoria(String nomeCategoria) {
		this.nomeCategoria = nomeCategoria;
	}


	public String getDescrizione() {
		return descrizione;
	}


	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
	
	
	
	
	
	
	
	
	
	
	
	

}
