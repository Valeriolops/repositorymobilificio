package com.mobilificio.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.mobilificio.connessioni.ConnettoreDB;
import com.mobilificio.model.Categoria;

public class CategoriaDAO implements Dao<Categoria> {

	@Override
	public Categoria getById(int id) throws SQLException {

		Connection conn = ConnettoreDB.getIstanza().getConnessione();

		String query = "select idCategoria, nomeCategoria,  descrizione  from categoria  where idCategoria = ?";

		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setInt(1, id);

		ResultSet risultato = ps.executeQuery();
		risultato.next();

		Categoria cTemp = new Categoria();
		cTemp.setIdCategoria(id);
		cTemp.setNomeCategoria(risultato.getString(2));
		cTemp.setDescrizione(risultato.getString(3));

		return cTemp;
	}

	@Override
	public ArrayList<Categoria> getAll() throws SQLException {

		ArrayList<Categoria> elenco = new ArrayList<Categoria>();

		Connection conn = ConnettoreDB.getIstanza().getConnessione();

		String query = "select idCategoria, nomeCategoria,  descrizione  from categoria ";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);

		ResultSet risultato = ps.executeQuery();
		while (risultato.next()) {

			Categoria cTemp = new Categoria();
			cTemp.setIdCategoria(risultato.getInt(1));
			cTemp.setNomeCategoria(risultato.getString(2));
			cTemp.setDescrizione(risultato.getString(3));
			elenco.add(cTemp);

		}

		return elenco;
	}

	@Override
	public void insert(Categoria objCategoria) throws SQLException {

		Connection conn = ConnettoreDB.getIstanza().getConnessione();

		String query = "   insert into Categoria  (nomeCategoria, descrizione ) value ( ?, ? )";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
		ps.setString(1, objCategoria.getNomeCategoria());
		ps.setString(2, objCategoria.getDescrizione());

		ps.executeUpdate();
		ResultSet risultato = ps.getGeneratedKeys();
		risultato.next();
		objCategoria.setIdCategoria(risultato.getInt(1));

	}

	@Override
	public boolean delete(Categoria objCategoria) throws SQLException {

		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		String query = "DELETE FROM categoria  WHERE idCategoria = ?";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setInt(1, objCategoria.getIdCategoria());

		int affRows = ps.executeUpdate();
		if (affRows > 0)
			return true;

		return false;

	}

	@Override
	public boolean update(Categoria t) throws SQLException {

		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		String query = "UPDATE categoria  SET " + " nomeCategoria  = ?, " + "descrizione = ? "
				+ "WHERE idCategoria = ?";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setString(1, t.getNomeCategoria());
		ps.setString(2, t.getDescrizione());
		ps.setInt(3, t.getIdCategoria());

		int affRows = ps.executeUpdate();
		if (affRows > 0)
			return true;

		return false;
	}

}
