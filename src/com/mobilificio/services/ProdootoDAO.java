package com.mobilificio.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.mobilificio.connessioni.ConnettoreDB;

import com.mobilificio.model.Prodotto;

public class ProdootoDAO  implements Dao<Prodotto>{

	@Override
	public Prodotto getById(int id) throws SQLException {

		Connection conn = ConnettoreDB.getIstanza().getConnessione();

		String query = " select idProdotto, nomeProdotto,descrizioneProdotto,codiceOggetto,prezzoVendita from Prodotto where idProdotto = ? ";

		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setInt(1, id);

		ResultSet risultato = ps.executeQuery();
		risultato.next();
		Prodotto pTemp = new Prodotto();
		pTemp.setIdProdotto(id);
		pTemp.setNomeProdotto(risultato.getString(2));
		pTemp.setDescrizioneProdotto(risultato.getString(3));
		pTemp.setCodiceOggetto(risultato.getString(4));
		pTemp.setPrezzoVendita(risultato.getDouble(5));
		return pTemp;
		
	
	}

	@Override
	public ArrayList<Prodotto> getAll() throws SQLException {
		
		
		ArrayList<Prodotto> elenco = new ArrayList<Prodotto>();

		Connection conn = ConnettoreDB.getIstanza().getConnessione();

		String query = "select idProdotto, nomeProdotto,descrizioneProdotto,codiceOggetto,prezzoVendita from Prodotto ";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);

		ResultSet risultato = ps.executeQuery();
		while (risultato.next()) {

			Prodotto pTemp = new Prodotto();
			pTemp.setIdProdotto(risultato.getInt(1));
			pTemp.setNomeProdotto(risultato.getString(2));
			pTemp.setDescrizioneProdotto(risultato.getString(3));
			pTemp.setCodiceOggetto(risultato.getString(4));
			pTemp.setPrezzoVendita(risultato.getDouble(5));
			elenco.add(pTemp);
			
		

		}

		return elenco;
		
		
		
		
	}

	@Override
	public void insert(Prodotto t) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();

		String query = "  insert into Prodotto  (nomeProdotto,descrizioneProdotto, codiceOggetto,prezzoVendita ) value ( ? , ? ,?, ?)";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
		ps.setString(1,t.getNomeProdotto());
		ps.setString(2, t.getDescrizioneProdotto());
		ps.setString(3, t.getCodiceOggetto());
		ps.setDouble(4, t.getPrezzoVendita());
	
		ps.executeUpdate();
		ResultSet risultato = ps.getGeneratedKeys();
		risultato.next();
		t.setIdProdotto(risultato.getInt(1));
		
	}

	@Override
	public boolean delete(Prodotto t) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		String query = "DELETE FROM Prodotto  WHERE idProdotto = ?";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setInt(1, t.getIdProdotto());

		int affRows = ps.executeUpdate();
		if (affRows > 0)
			return true;

		return false;
	}

	@Override
	public boolean update(Prodotto t) throws SQLException {
		Connection conn = ConnettoreDB.getIstanza().getConnessione();
		String query = "UPDATE Prodotto  SET " + " nomeProdotto  = ?, " + "descrizioneProdotto = ? "+ " codiceOggetto = ? "+ "prezzoVendita = ? " +" WHERE idProdotto = ?";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setString(1, t.getNomeProdotto());
		ps.setString(2, t.getDescrizioneProdotto());
		ps.setString(3, t.getCodiceOggetto());
		ps.setDouble(4, t.getPrezzoVendita());
		ps.setInt(5, t.getIdProdotto());
		

		int affRows = ps.executeUpdate();
		if (affRows > 0)
			return true;

		return false;
	}

}
